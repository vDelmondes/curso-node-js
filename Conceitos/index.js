//Objeto
let objeto = [{
  "nome": "João Vitor D.",
  "email": "vitord@gmail.com",
  "idade": "20"
},
{
  "nome": "Luizêra",
  "email": "luizera@gmail.com",
  "idade": "19"
},
{
  "nome": "Alekêy",
  "email": "alekey@gmail.com",
  "idade": "23"
}];

//Array
let array = [1, 2, 3, 4];

//Classe
class LuizCornoSafado {
  constructor(param) {
    if (param == "safado" || param == "bandido") {
      console.log("certo!");
    } else {
      console.log("Nada disso");
    }
  }
}

// let luiz = new LuizCornoSafado("bandido");

// async function sleep ( param ) {
//   //Promisse
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       resolve();
//     }, param);
//   })
// }
// sleep(2000).then(() => {
//   console.log("iep");
// })

function sleep( param ){
  return new Promise((resolve, reject) =>{
    setTimeout(resolve, param);
  })
}
sleep(2000).then(() => {
  val();
})

function val() {
  console.log("Cabei,.");
}





